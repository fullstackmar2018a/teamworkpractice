﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace LMS.Model
{
    public class LMSDataStore:ILMSDataStore
    {
        private LMSDBContext _ctx;

        public LMSDataStore(LMSDBContext ctx)
        {
            _ctx = ctx;
        }
        // For Course API 
        public IEnumerable<Course> GetAllCourses()
        {
            return _ctx.Courses.OrderBy(course => course.Id).ToList();
        }

        public Course GetCourse(int courseID){
            return _ctx.Courses.Find(courseID);
        }


        public void AddCourse(Course course)
        {
            _ctx.Courses.Add(course);
            Save();
        }

        public void EditCourse(int courseID, Course course){
            Course courseToEdit = _ctx.Courses.Find(courseID);
            course.Name = course.Name;
            //TODO: Replace the rest Attrubte
            Save();
        }

        // For Student API 
        public IEnumerable<Student> GetAllStudent()
        {
            var result = _ctx.Students
                             .Include(s => s.StudentDetail)
                             .Include(s=> s.Enrollments).ThenInclude(cw=>cw.Course)
                             .OrderBy(student => student.Id).ToList();

            return result;
        }

        public void AddStudent(Student student)
        {
            _ctx.Students.Add(student);
        }

        // For Enrolment API 
        public void AddEnrolment(int studentId, int courseId){
            Student student = _ctx.Students.Find(studentId);
            Course course = _ctx.Courses.Find(courseId);

            var newEnrol = new Enrolment {StudentId=studentId,CourseId=courseId};
            _ctx.Enrolments.Add(newEnrol);
            Save();
        }

        public void RemoveEnrolment(int studentID, int courseID){
            var enrol = _ctx.Enrolments.Find(courseID,studentID);
            if(enrol != null) {
                _ctx.Enrolments.Remove(enrol);
            }
            Save();
        }


        public bool Save()
        {
            //True for success , False should throw exception
            return (_ctx.SaveChanges() >= 0);
        }

    }
}

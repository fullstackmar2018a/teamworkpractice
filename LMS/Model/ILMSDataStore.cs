﻿using System;
using System.Collections.Generic;


namespace LMS.Model
{
    public interface ILMSDataStore
    {
        IEnumerable<Course> GetAllCourses();
        Course GetCourse(int courseID);
        void AddCourse(Course course);
        void EditCourse(int courseID, Course course);

        IEnumerable<Student> GetAllStudent();
        void AddStudent(Student student);


        void AddEnrolment(int studentID, int courseID);
        void RemoveEnrolment(int studentID, int courseID);
        bool Save();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using LMS.Model;

namespace LMS.Controllers
{
    [Route("api/courses")]
    public class CourseController : Controller
    {
        private ILMSDataStore _dbstore;
        public CourseController(ILMSDataStore dbstore)
        {
            _dbstore = dbstore;
        }


        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_dbstore.GetAllCourses());
        }


        [HttpGet("{courseID}")]
        public IActionResult Get(int courseID)
        {
            IActionResult result;
            var course = _dbstore.GetCourse(courseID);
            if(course != null) {
                result = Ok(course);
            } else {
                result = NotFound();
            }
            return result;
        }

        [HttpPost]
        public IActionResult Post([FromBody]Course input)
        {
            Course newCourse = Course.CreateNewCourseFromBody(input);
            _dbstore.AddCourse(newCourse);
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Put(int courseID,[FromBody]Course input)
        {
            Course newCourse = Course.CreateNewCourseFromBody(input);
            _dbstore.EditCourse(courseID,newCourse);
            return Ok();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
